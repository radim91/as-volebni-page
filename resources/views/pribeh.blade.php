@extends('layouts.app')

@section('title')
    Můj životní příběh
@endsection

@section('content')
    <div class="container-xl">

        <div class="row">
            <div class="col-xl-7">
                <img src="{{ asset('images/pribeh.png') }}" alt="můj příběh" class="page-head">
            </div>
        </div>

        <div class="row mt-5 mb-5">
            <div class="col-xl-7">
                <p>
                    Narodila jsem se 23. června 1951 v Brně. Po absolvování gymnázia na Slovanském náměstí jsem začala v roce 1969 studovat filozofii a historii na brněnské filozofické fakultě. Ve třetím ročníku jsem byla zatčena a odsouzena za distribuci letáků k nedemokratickým volbám. Po propuštění jsem už nemohla dostudovat a pracovala jako úřednice.
                </p>
                <p>
                    Jako jedna z prvních v roce 1976 jsem podepsala Chartu 77 a v roce 1986 se stala její mluvčí. V roce 1978 jsem spoluzakládala Výbor na obranu nespravedlivě stíhaných, od roku 1987 pak byla mluvčí iniciativy Polsko-československá solidarita.
                </p>
                <p>
                    Po roce 1989 jsem působila jako poradkyně ministra práce a sociálních věcí. V roce 1996 dokončila magisterské studium českého jazyka a literatury a pracovala jako redaktorka, později jako vedoucí Městského centra sociálních služeb. V letech 2001 až 2007 jsem zastávala funkci zástupkyně veřejného ochránce práv Otakara Motejla, v letech 2014 až 2020 jsem pak působila jako ombudsmanka.
                </p>
                <p>
                    V letech 2008–2013 předsedala Českému helsinskému výboru (čestná funkce), v letech 2008 až 2011 jsem byla za Českou republiku členkou Evropského výboru pro zabránění mučení. V roce 2002 mne prezident Václav Havel vyznamenal medailí Za zásluhy I. stupně. Dále jsem držitelkou Ceny OSN za obranu lidských práv, ceny Alice Garrigue Masarykové udělované velvyslancem USA a Velitelského kříže Řádu za zásluhy Polské republiky. V roce 2018 mi francouzský prezident Emmanuel Macron propůjčil nejvyšší státní vyznamenání Řád čestné legie v hodnosti rytířky.
                </p>

                <div class="row mt-3">
                    <div class="col-sm-6">
                        <img src="{{ asset('images/pribeh/foto1.png') }}" alt="Fotografie 1" width="100%" class="pb-2">
                    </div>

                    <div class="col-sm-6">
                        <img src="{{ asset('images/pribeh/foto2.png') }}" alt="Fotografie 2" width="100%" class="pb-2">
                        <img src="{{ asset('images/pribeh/foto3.png') }}" alt="Fotografie 3" width="100%">
                    </div>
                </div>

                <h3 class="mt-5 mb-4 border-bottom-teal">„HANKA“</h3>
                <p>
                    Pro Annu – neboli Hanku, jak jí říká rodina a přátelé – byl celoživotním vzorem její otec Jaroslav,
                    vysokoškolský učitel, politický filozof, významný aktér reformního hnutí v roce 1968 a polistopadový
                    ministr.
                </p>
                <p>
                    V roce 1974 se Hanka provdala za Petra Uhla, s nímž má tři děti, Pavla, Alexandru a Michala, a sedm
                    vnoučat. Její dcera Saša vzpomíná, že i v nejtěžších dobách, kdy byl otec ve vězení, dokázala Hanka
                    vytvořit v rodině atmosféru bezpečí. Intenzivní péče o blízké je samozřejmou součástí Hančina života
                    dodnes a její aktivity na podporu rodinné péče se tedy opírají nejen o profesní poznatky, ale hlavně o
                    silnou osobní zkušenost.
                </p>

                <div class="row">
                    <div class="col-sm-6">
                        <img src="{{ asset('images/pribeh/foto4.png') }}" alt="Fotografie 4" width="100%" class="pb-2">
                    </div>

                    <div class="col-sm-6">
                        <img src="{{ asset('images/pribeh/foto5.png') }}" alt="Fotografie 5" width="100%">
                    </div>
                </div>
            </div>

            <div class="col-xl-5 mb-5 text-center">
                <div class="py-4">
                    @include('layouts.buttons')
                </div>

                <div class="mt-5">
                    <facebook></facebook>
                </div>

                <div class="mt-5 mt-5">
                    @include('layouts.endorsement')
                </div>
            </div>
        </div>
    </div>
@endsection
