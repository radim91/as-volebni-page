@extends('layouts.app')

@section('title')
    Přihlásit se jako dobrovolník
@endsection

@section('content')

    <div class="container-xl">

        <div class="row">

            <div class="col-xl-12 text-center mt-5">
                <h2>Chcete pomoct Anně v kampani?<br>Přihlašte se!</h2>
            </div>

            <div class="col-xl-12 mt-5">

                <form method="POST">
                    @csrf
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label">Jméno*</label>

                        <div class="col-md-8">
                            <input type="text" class="form-control" name="name" placeholder="Jméno" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="surname" class="col-md-4 col-form-label">Příjmení*</label>

                        <div class="col-md-8">
                            <input type="text" class="form-control" name="surname" placeholder="Příjmení" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label">E-mail*</label>

                        <div class="col-md-8">
                            <input
                                type="email"
                                class="form-control"
                                name="email"
                                placeholder="vas@email.cz"
                                required
                            />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="phone" class="col-md-4 col-form-label">Telefon</label>

                        <div class="col-md-8">
                            <input type="text" class="form-control" name="phone" placeholder="+420 XXX XXX XXX" />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="address" class="col-md-4 col-form-label">Adresa</label>

                        <div class="col-md-8">
                            <input type="text" class="form-control" name="address" placeholder="ul. Brněnská, Brno" />
                        </div>
                    </div>

                    <hr class="mt-5 mb-5">

                    <div class="form-group row">
                        <div class="col-sm-4"><strong>Čím byste kampaň podpořil/a?</strong></div>
                        <div class="col-sm-8 py-1">
                            <div class="form-check mt-2">
                                <input class="form-check-input" type="checkbox" name="pomoc_letaky" id="letaky">
                                <label class="form-check-label" for="letaky">
                                    Roznesu letáky do schránek v okolí
                                </label>
                                <input type="text" name="pomoc_letaky_adr" class="form-control mt-1" placeholder="Kde?">
                            </div>

                            <div class="form-check mt-2">
                                <input class="form-check-input" type="checkbox" name="pomoc_stanek" id="stanek">
                                <label class="form-check-label" for="stanek">
                                    Postavím se ke stánku nebo na ulici a budu rozdávat
                                </label>
                                <input type="text" name="pomoc_stanek_adr" class="form-control mt-1" placeholder="Kde?">
                            </div>

                            <div class="form-check mt-2">
                                <input class="form-check-input" type="checkbox" name="pomoc_jine" id="jine">
                                <label class="form-check-label" for="jine">
                                    Mám jiný nápad
                                </label>
                                <textarea name="pomoc_jine_detail" class="form-control mt-1">Jaký?</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row mt-4">
                        <div class="offset-md-4 col-md-6 mt-1">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="gdpr" id="gdpr" required>
                                <label class="form-check-label" for="gdpr">
                                    Souhlasím se zpracováním osobních údajů
                                </label>
                            </div>
                        </div>

                        <div class="col-md-2 mt-1 text-right">
                            <button type="submit" class="btn btn-primary">Přihlásit se</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
