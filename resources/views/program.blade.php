@extends('layouts.app')

@section('title')
    Program
@endsection

@section('content')
    <div class="container-xl">

        <div class="row pl-2 pr-2">
            <div class="col-xl-7 mt-5">
                <img src="{{ asset('images/program.png') }}" alt="můj program" class="page-head">
            </div>

            <div class="col-xl-5 mt-5">
                <div class="row mt-3 text-left">
                    <div class="col-4">
                        <a href="#socialni-sluzby" class="handwritten bigger">SOCIÁLNÍ<br>SLUŽBY</a>
                    </div>

                    <div class="col-4 text-center">
                        <a href="#zivotni-prostredi" class="handwritten bigger">ŽIVOTNÍ<br>PROSTŘEDÍ</a>
                    </div>

                    <div class="col-4 text-right">
                        <a href="#spravedlnost" class="handwritten bigger">SPRAVE-<br>DLNOST</a>
                    </div>
                </div>
            </div>

            <div class="col-xl-7 mt-5 mb-5">
                <p>
                    Při projednávání a schvalování zákonů Senátem budu dbát na zachování všech hodnot demokratického právního státu. Pozice senátorky mi umožní aktivně promlouvat do debaty na půdě Parlamentu a hledat spolu s kolegy podporu pro návrhy, které reagují na potřeby naší společnosti. Zvláštní pozornost věnuji sociálním službám, ochraně přírody a základním právům každého člověka.
                </p>
                <p>
                    Jako senátorka zvolená za Brno budu přispívat i k řešení místních problémů, zprostředkovávat setkání, propojovat aktéry z různých oblastí, podporovat aktivity místních organizací, spolků i jednotlivců.
                </p>

                <h2 id="socialni-sluzby" class="mt-5 border-bottom-teal"><strong>Sociální služby</strong> jako opora pro celý život</h2>

                <p class="py-4">
                    <strong>Sociální oblast stát dlouhodobě opomíjí, přestože jde o jednu z jeho nejdůležitějších rolí.</strong><br>
                    Naše společnost prochází výraznou demografickou proměnou, přibývá lidí závislých na péči druhých. Může se to týkat každého z nás. Pokud chceme být moderní humanistickou společností, musíme změnit obsah a formu sociální péče a soustředit se přitom na individuální potřeby každého člověka. Péči musíme deinstitucionalizovat, tedy opustit velká neosobní zařízení a přejít na terénní péči, která lidem umožní žít v jejich vlastním prostředí, v rodině nebo v malých společenstvích.
                </p>

                <p>
                    Sociální péče není izolovaným odvětvím, prostupuje mnoha dalšími oblastmi našeho života, od zdravotnictví přes architekturu a urbanismus, a poskytuje nám oporu doslova od narození do smrti.
                </p>

                <h4>Co chci prosazovat a podporovat v Senátu</h4>
                <ul>
                    <li>
                        lepší návaznost a provázanost zdravotních a sociálních služeb
                    </li>
                    <li>
                        dostupnost všech odlehčovacích služeb a terénních služeb a rozšíření jejich poskytování až na 24 hodin denně
                    </li>
                    <li>
                        mezigenerační solidaritu, sousedskou výpomoc, spolkovou činnost, komunitní projekty, jinými slovy rozvoj občanské společnosti a dobrovolnictví
                    </li>
                    <li>
                        zrychlení přechodu z ústavní péče do péče individuální (místo kojeneckých ústavů pěstounská péče, místo velkokapacitních pobytových zařízení komunitní péče o seniory a lidi s postižením, chráněná bydlení, individuální bydlení s osobní asistencí…)
                    </li>
                    <li>
                        budování bezbariérových prostorů, včetně nových bytů, aby budoucí generace mohly co nejdéle žít ve vlastním prostředí
                    </li>
                    <li>
                        spravedlivé finanční ohodnocení pracovníků v sociální oblasti a neformálních (rodinných) pečujících
                    </li>
                    <li>
                        prosazování práv klientů sociálních služeb
                    </li>
                </ul>

                <h4>Co chci podporovat v Brně</h4>

                <ul>
                    <li>
                        výstavbu malých, bezbariérových a cenově dostupných bytů pro seniory, lidi s postižením, neúplné rodiny…
                    </li>
                    <li>
                        dostupnost sociálních služeb
                    </li>
                    <li>
                        dostupnost paliativní péče v každé městské části umožňující lidem se závažným a neléčitelným onemocněním žít v domácím prostředí
                    </li>
                    <li>
                        transformaci velkých ústavních zařízení sociálních služeb s cílem umožnit klientům přechod do komunitních služeb
                    </li>
                    <li>
                        smysluplné čerpání prostředků z Evropské unie pro transformaci a modernizaci sociálních služeb
                    </li>
                </ul>

                <h2 class="mt-5 border-bottom-teal" id="zivotni-prostredi"><strong>Ochrana životního prostředí</strong> jako podmínka udržení života</h2>
                <p class="py-4">
                    O nutnosti aktivně chránit životní prostředí už dnes nepochybuje snad nikdo. Tři čtvrtiny obyvatel České republiky považují klimatické změny za velký problém a obávají se, že kvůli nim bude život složitější. Tíživé dopady sledujeme už dnes – lidé trpí horkem ve špatně izolovaných domech, vysychají studny a další vodní zdroje, lesy nám mizí před očima.
                </p>
                <p>
                    Mým cílem je prosazovat klimatickou spravedlnost.
                </p>

                <h4>Co chci podporovat v Senátu</h4>
                <ul>
                    <li>
                        využití prostředků z Evropské unie do moderních technologií (obnovitelných zdrojů energie, inovativních zelených technologií šetrných k životnímu prostředí), postupné promyšlené ukončení uhlíkové ekonomiky (při obnově ekonomiky po covidové epidemii nelze s klimatickými opatřeními čekat, „až se ekonomika uzdraví“)
                    </li>

                    <li>
                        opatření k předcházení dopadů změny klimatu (snižování emisí skleníkových plynů)
                    </li>

                    <li>
                        opatření k přizpůsobení se změnám klimatu (zvýšit dostupnost opatření, jež umožní kvalitní život v nových klimatických podmínkách)
                    </li>

                    <li>
                        zpřísnění ochrany vodních zdrojů, zejména zásob pitné vody
                    </li>

                    <li>
                        zvýšení evropských emisních cílů 2030
                    </li>

                    <li>
                        rozumné nakládání s dešťovou vodou
                    </li>
                </ul>

                <h4>Co chci podporovat v Brně</h4>

                <ul>
                    <li>
                        mezioborové debaty o dopadech klimatické změny za účasti klimatologů, sociálních pracovníků, architektů, urbanistů, sociologů atd.
                    </li>

                    <li>
                        chytré územní plánování
                    </li>
                </ul>

                <h2 class="mt-5 border-bottom-teal" id="spravedlnost"><strong>Spravedlnost pro každého</strong> jako součást lidských práv</h2>
                <p class="py-4">
                    Musíme investovat do soudržné společnosti, v níž se společné bohatství rozděluje spravedlivě. Do společnosti, která vychází z konceptu lidských práv a nepomíjí ani práva sociální. Sociální rozdíly se nesmějí zvětšovat, ale zmenšovat.
                </p>

                <h4>Na co nesmíme zapomínat</h4>
                <ul>
                    <li>
                        Bydlení je základní lidská potřeba, nejen tržní komodita. Potřebujeme zákon o sociálním bydlení.
                    </li>
                    <li>
                        Lidem, kteří desítky let pracovali, nelze odepřít spravedlivý a důstojný důchod. Musíme snížit dosavadní limit 35 let důchodového pojištění – „odpracovaných let“.
                    </li>
                    <li>
                        Exekuce jsou především výkonem státní moci, a ne byznysem. Proto bychom měli zavést územní působnost soudních exekutorů.
                    </li>
                    <li>
                        Při projednávání zákonů musí Parlament ctít principy demokratického právního státu a ochrany základních práv a svobod.
                    </li>
                    <li>
                        Demokratické instituce, jako je Ústavní soud, veřejný ochránce práv nebo Úřad pro ochranu osobních údajů, musíme chránit a podporovat.
                    </li>
                </ul>
                <img src="{{ asset('images/program-fotka.png') }}" alt="můj program fotografie" width="100%" class="mt-4">
            </div>

            <div class="col-xl-5 mb-5 text-center">
                <div class="py-4">
                    @include('layouts.buttons')
                </div>

                <div class="mt-5">
                    <facebook></facebook>
                </div>

                <div class="mt-5 mt-5">
                    @include('layouts.endorsement')
                </div>
            </div>
        </div>
    </div>
@endsection
