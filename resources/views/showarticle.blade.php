@extends('layouts.app')

@section('title')
    {{ $article->name }}
@endsection

@section('content')
    <div class="container-xl">
        <div class="row mt-5 mb-5">
            <div class="col-xl-7">

                <h1 class="border-bottom-teal mt-5 mb-5">{{ $article->name }}</h1>
                {!! $article->content !!}
                <img src="{{ '/storage/'.$article->header }}" alt="{{ $article->slug }}" width="100%" class="py-5">
            </div>

            <div class="col-xl-5 mt-5 mb-5 text-center">
                <div class="py-4">
                    @include('layouts.buttons')
                </div>

                <div class="mt-5">
                    <facebook></facebook>
                </div>

                <div class="mt-5 mt-5">
                    @include('layouts.endorsement')
                </div>
            </div>
        </div>
    </div>
@stop
