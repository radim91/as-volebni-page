<nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
    <a class="navbar-brand" href="/">Mgr. <strong>ANNA ŠABATOVÁ</strong> Ph.D.</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarToggler">
        <ul class="navbar-nav ml-auto text-right">
            <li class="nav-item">
                <a class="nav-link @if (Request::is("pribeh")) nav-link-active @endif" href="{{ route('pribeh') }}">MŮJ PŘÍBĚH</a>
            </li>
            &nbsp;
            <li class="nav-item">
                <a class="nav-link @if (Request::is("program") || Request::is("vize")) nav-link-active @endif" href="{{ route('program') }}">PROGRAM</a>
            </li>
            &nbsp;
            <li class="nav-item">
                <a class="nav-link @if (Request::is("podpora")) nav-link-active @endif" href="{{ route('podpora') }}">PODPOROVATELÉ</a>
            </li>
        </ul>
    </div>
</nav>
