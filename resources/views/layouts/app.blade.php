<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta property="og:url"                content="https://sabatovadosenatu.cz" />
    <meta property="og:type"               content="article" />
    <meta property="og:title"              content="@yield('title') - Šabatová do Senátu" />
    <meta property="og:description"        content="Jsem nestranička, někdejší veřejná ochránkyně práv, mluvčí Charty 77 a spoluzakladatelka Výboru na obranu nespravedlivě stíhaných. " />
    <meta property="og:image"              content="{{ asset('images/facebook-og.png') }}" />

    <title>@yield('title') - {{ config('app.name') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/cs_CZ/sdk.js#xfbml=1&autoLogAppEvents=1&version=v8.0&appId=452918734873072" nonce="oWhYaYzP"></script>
    @if (Request::is('*admin*'))
        <script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
    @endif

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        @include('layouts.navbar')

        <main class="py-2">
                @yield('content')
        </main>
        @include('layouts.footer')
    </div>

</body>
</html>
