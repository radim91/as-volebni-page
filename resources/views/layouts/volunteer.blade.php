      <form method="POST">
          @csrf
        <div class="form-group row">
          <label for="name" class="col-xm-6 col-form-label">Jméno</label>

          <div class="col-sm-6">
            <input type="text" class="form-control" name="name" placeholder="Jméno" required />
          </div>
        </div>

        <div class="form-group row">
          <label for="surname" class="col-xm-6 col-form-label">Příjmení</label>

          <div class="col-sm-6">
            <input type="text" class="form-control" name="surname" placeholder="Příjmení" required />
          </div>
        </div>

        <div class="form-group row">
          <label for="email" class="col-xm-6 col-form-label">E-mail</label>

          <div class="col-sm-6">
            <input
              type="email"
              class="form-control"
              name="email"
              placeholder="vas@email.cz"
              required
            />
          </div>
        </div>

        <div class="form-group row">
          <label for="phone" class="col-xm-6 col-form-label">Telefon</label>

          <div class="col-sm-6">
            <input type="text" class="form-control" name="phone" placeholder="+420 XXX XXX XXX" />
          </div>
        </div>

        <div class="form-group row">
          <label for="address" class="col-xm-6 col-form-label">Adresa</label>

          <div class="col-sm-6">
            <input type="text" class="form-control" name="address" placeholder="ul. Brněnská, Brno" />
          </div>
        </div>
      </form>

    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Zavřít</button>
      <button type="button" class="btn btn-primary">Zaregistrovat se</button>
    </div>
