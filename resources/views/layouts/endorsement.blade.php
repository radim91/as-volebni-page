<h3 class="handwritten teal-big">PODPORUJÍ MĚ</h3>
<img src="{{ asset('images/rozdelovnik-teal.png') }}" alt="rozdelovnik teal" width="150" height="auto">

<div class="row mt-4">
    <div class="col-sm-6">
        <img src="{{ asset('images/podpora/wagnerova.png') }}" alt="Wagnerová" class="endorsement-img">
    </div>

    <div class="col-sm-6 text-left">
        <h5 class="teal">
            <a href="/podpora#wagnerova">Eliška Wagnerová</a>
        </h5>
        emeritní ústavní soudkyně a senátorka
    </div>
</div>

<div class="row mt-4">
    <div class="col-sm-6">
        <img src="{{ asset('images/podpora/stredula.png') }}" alt="Středula" class="endorsement-img">
    </div>

    <div class="col-sm-6 text-left py-3">
        <h5 class="teal"><a href="/podpora#stredula">Josef Středula</a></h5>
        předseda českých odborů (ČMKOS)
    </div>
</div>

<div class="row mt-4">
    <div class="col-sm-6">
        <img src="{{ asset('images/podpora/pithart.png') }}" alt="Pithart" class="endorsement-img">
    </div>

    <div class="col-sm-6 text-left py-3">
        <h5 class="teal"><a href="/podpora#pithart">Petr Pithart</a></h5>
        bývalý premiér a předseda Senátu
    </div>
</div>

<div class="row mt-4">
    <div class="col-sm-6">
        <img src="{{ asset('images/podpora/schwarzenberg.png') }}" alt="Schwarzenberg" class="endorsement-img">
    </div>

    <div class="col-sm-6 text-left">
        <h5 class="teal"><a href="/podpora#schwarzenberg">Karel Schwarzenberg</a></h5>
        bývalý ministr zahraničí a senátor
    </div>
</div>

<div class="row mt-4">
    <div class="offset-sm-6 col-sm-6 text-left">
        <a href="{{ route('podpora') }}">a další...</a>
    </div>
</div>
