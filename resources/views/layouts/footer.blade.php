<div class="container-xl">
    <footer class="border-top-teal mt-5 mb-5">
        <div class="row justify-content-center mt-4">
            <div class="col-xl-6 text-center">
                <a href="https://www.facebook.com/sabatovaanna/" target="_blank">
                    <img src="/images/fb.png" alt="fb" width="25px" height="auto">
                </a>
                <a href="http://instagram.com/anna.sabatova">
                    <img src="/images/ig.png" width="25px" height="auto">
                </a>

                <a href="mailto:info.annasabatova@gmail.com">
                    <img src="{{ asset('images/mail.png') }}" alt="email" width="30px" height="auto">
                </a>
            </div>

            <div class="col-xl-6 text-center">
                <small>Zadavatel: Zelení, SEN21, Idealisté<br>
                    Zpracovatel: Breisky, Idealisté</small>
            </div>
        </div>
    </footer>

</div>

