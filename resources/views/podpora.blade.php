@extends('layouts.app')

@section('title')
    Podporují mě
@endsection

@section('content')
    <div class="container-xl">

        <div class="row">
            <div class="col-xl-7" id="wagnerova">
                <img src="{{ asset('images/podpora.png') }}" alt="podpora" class="page-head">
            </div>
        </div>

        <div class="row mt-5 mb-5">
            <div class="col-xl-7">
                <div class="row">
                    <div class="col-sm-4" id="pithart">
                        <img src="{{ asset('images/podpora/wagnerova.png') }}" alt="Eliška Wagnerová" class="endorsement-img">
                    </div>

                    <div class="col-sm-8">
                        <blockquote class="blockquote">
                            <p class="mb-0">
                                Je to slušný a férový člověk, který celý život stojí za principy i za cenu újmy na vlastní svobodě.
                            </p>
                            <footer class="blockquote-footer teal py-3"><strong>Eliška Wagnerová</strong>, emeritní ústavní soudkyně a senátorka</footer>
                        </blockquote>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4" id="schwarzenberg">
                        <img src="{{ asset('images/podpora/pithart.png') }}" alt="Petr Pithart" class="endorsement-img">
                    </div>

                    <div class="col-sm-8">
                        <blockquote class="blockquote">
                            <p class="mb-0">
                                Dal bych za ni ruku do ohně, a to jen tak o někom říct nemůžu. Znám ji skoro půl století, za tu dobu má jednu základní starost, stále stejnou, a to starost nejobtížnější. Starost o slabé, nejprve nespravedlivě stíhané, pak ublížené a zapomenuté. Zná sociální systém, a tedy i právní, který je plný nedostatků.
                            </p>
                            <footer class="blockquote-footer teal py-3"><strong>Petr Pithart</strong>, bývalý premiér a předseda Senátu</footer>
                        </blockquote>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4" id="stredula">
                        <img src="{{ asset('images/podpora/schwarzenberg.png') }}" alt="Karel Schwarzenberg" class="endorsement-img">
                    </div>

                    <div class="col-sm-8">
                        <blockquote class="blockquote">
                            <p class="mb-0">
                                Podporuji ji, protože ji znám přes 30 let. Je to velká bojovnice za lidská práva a za demokracii. Nemáme stejné politické názory, ale považuji ji za člověka, který je nutný a potřebný v českém Senátu.
                            </p>
                            <footer class="blockquote-footer teal py-3"><strong>Karel Schwarzenberg</strong>, bývalý ministr zahraničí a senátor</footer>
                        </blockquote>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <img src="{{ asset('images/podpora/stredula.png') }}" alt="Josef Středula" class="endorsement-img">
                    </div>

                    <div class="col-sm-8">
                        <blockquote class="blockquote">
                            <p class="mb-0">
                                Senát potřebuje osobnosti, jejichž životním krédem je zastávat se lidí, pomáhat jim a vnímat jejich křivdy. To Anna Šabatová plně prokázala ve své funkci Ombudsmanky. Věřím, že voliči rozhodnou, aby na roli ochránce práv mohla navázat v Senátu. Podporuji její kandidaturu a přeji co nejširší podporu voličů.
                            </p>
                            <footer class="blockquote-footer teal py-3"><strong>Josef Středula</strong>, předseda českých odborů (ČMKOS)</footer>
                        </blockquote>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <img src="{{ asset('images/podpora/tuckova.png') }}" alt="Kateřina Tučková" class="endorsement-img">
                    </div>

                    <div class="col-sm-8">
                        <blockquote class="blockquote">
                            <p class="mb-0">
                                Léta jsem vděčně sledovala její angažmá ve prospěch slabých a ostrakizovaných, a mám za to, že by teď bylo nejlepší, kdyby své letité zkušenosti uplatnila ve prospěch nás všech – coby příští senátorka.
                            </p>
                            <footer class="blockquote-footer teal py-3"><strong>Kateřina Tučková</strong>, brněnská spisovatelka</footer>
                        </blockquote>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <img src="{{ asset('images/podpora/spidla.png') }}" alt="Vladimír Špidla" class="endorsement-img">
                    </div>

                    <div class="col-sm-8">
                        <blockquote class="blockquote">
                            <p class="mb-0">
                                Znám ji a vím, že je pevná osobnost s občanskou odvahou. Znám taky politiku a vím, že právě lidí, kteří jsou schopni hájit humanistické hodnoty, jsou v naší politice vzácní a potřební.
                            </p>
                            <footer class="blockquote-footer teal py-3"><strong>Vladimír Špidla</strong>, bývalý premiér</footer>
                        </blockquote>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <img src="{{ asset('images/podpora/kubisova.png') }}" alt="Marta Kubišová" class="endorsement-img">
                    </div>

                    <div class="col-sm-8">
                        <blockquote class="blockquote">
                            <p class="mb-0">
                                Aničku znám od roku ‘77. Je to nesmlouvavá bojovnice za právo a spravedlnost. Myslím, že pro Senát má málokterá žena takovou životní průpravu jako ona.
                            </p>
                            <footer class="blockquote-footer teal py-3"><strong>Marta Kubišová</strong>, zpěvačka</footer>
                        </blockquote>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-4">
                        <img src="{{ asset('images/podpora/maly.png') }}" alt="Václav Malý" class="endorsement-img">
                    </div>

                    <div class="col-sm-8">
                        <blockquote class="blockquote">
                            <p class="mb-0">
                                Anna se vždy v občanských iniciativách i ve službě veřejného ochránce práv zasazovala o spravedlnost a respekt k lidské důstojnosti. To je dobrý předpoklad pro působení i v politických strukturách.
                            </p>
                            <footer class="blockquote-footer teal py-3"><strong>Václav Malý</strong>, biskup</footer>
                        </blockquote>
                    </div>
                </div>

                <h3 class="mt-5 mb-5 border-bottom-teal">Annu Šabatovou do Senátu dále podporují:</h3>

                <ul>
                    <li>
                        herec Arnošt Goldflam
                    </li>

                    <li>
                        bývalý ministr Milan Uhde
                    </li>

                    <li>
                        ministr kultury Lubomír Zaorálek
                    </li>

                    <li>
                        rektor JAMU Petr Oslzlý
                    </li>

                    <li>
                        dokumentarista Břetislav Rychlík
                    </li>

                    <li>
                        děkan FI MU Jiří Zlatuška
                    </li>

                    <li>
                        básník Martin Reiner
                    </li>

                    <li>
                        ekonomka Naďa Johanisová
                    </li>

                    <li>
                        producentka Kamila Zlatušková
                    </li>

                    <li>
                        poslanec Roman Sklenák
                    </li>

                    <li>
                        starostka Milada Blatná
                    </li>

                    <li>
                        starostka Ivana Fajnorová
                    </li>

                    <li>
                        starosta Michal Šmarda
                    </li>

                    <li>
                        senátor Václav Láska
                    </li>

                    <li>
                        spolupředsedové Zelených Michal Berg a Magdalena Davis
                    </li>

                    <li>
                        publicista František Kostlán
                    </li>

                    <li>
                        psychoterapeutka Věra Roubalová Kostlánová
                    </li>

                    <li>
                        předseda Idealistů Vojtěch Vašák a další.
                    </li>
                </ul>

            </div>

            <div class="col-xl-5 mb-5 text-center">
                <div class="py-4">
                    @include('layouts.buttons')
                </div>

                <div class="mt-5">
                    <facebook></facebook>
                </div>

            </div>
        </div>
    </div>
@endsection
