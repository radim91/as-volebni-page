@extends('layouts.app')

@section('title')
    Úvod
@endsection

@section('content')

        <div class="container-fluid">

            <div class="row header justify-content-between">
                <div class="col-9">
                    <img src="{{ asset('images/header_nadpis.png') }}" alt="nadpis" class="header-nadpis">
                </div>
                <div class="col-3 header-text text-center">
                    <img src="{{ asset('images/header-vpravo.png') }}" alt="header vpravo" width="70%">
                </div>
            </div>
        </div>

        <div class="container-xl">

            <div class="row mt-5 justify-content-between">
                <div class="col-xl-8">
                    <div class="row">
                        <div class="col-xl-6 text-center mb-5">
                            @if (session('status'))
                                <div class="alert alert-success text-center">
                                    <p>
                                        Děkujeme za Vaši ochotu podpořit Annu Šabatovou.<br>
                                        V nejbližší době se Vám ozve člen volebního týmu.
                                    </p>
                                </div>
                            @else
                                @include('layouts.buttons')

                            @endif
                        </div>

                        <div class="col-xl-6">
                            <div>
                                <h6 class="teal border-bottom-teal">Kandidátka číslo 1<br>v obvodě č. 60 Brno-město</h6>
                                <p class="teal">
                                    Brno-sever, Žabovřesky, Jundrov, Komín, Královo Pole, Řečkovice a Mokrá Hora, Medlánky, Ivanovice, Jehnice, Ořešín a Útěchov.
                                </p>
                            </div>

                        </div>

                        <div class="col-xl-12 mb-5">
                            <h2 class="headline mb-4">Moje vize</h2>
                            <p>
                                Při projednávání a schvalování zákonů Senátem budu dbát na zachování všech hodnot demokratického právního státu. Pozice senátorky mi umožní aktivně promlouvat do debaty na půdě Parlamentu a hledat spolu s kolegy podporu pro návrhy, které reagují na potřeby naší společnosti. Zvláštní pozornost věnuji sociálním službám, ochraně přírody a základním právům každého člověka.
                            </p>
                            <p>
                                Jako senátorka zvolená za Brno budu přispívat i k řešení místních problémů, zprostředkovávat setkání, propojovat aktéry z různých oblastí, podporovat aktivity místních organizací, spolků i jednotlivců.
                            </p>

                            <div class="row mt-5 mb-3">
                                <div class="col-sm-4 text-center mb-3">
                                    <h4 class="handwritten big">
                                        <a href="/program#socialni-sluzby" class="black">SOCIÁLNÍ<br>SLUŽBY</a><br>
                                        <img src="{{ asset('images/rozdelovnik-cerny-2.png') }}" alt="rozdelovnik" width="100px">
                                    </h4>
                                </div>
                                <div class="col-sm-4 text-center mb-3">
                                    <h4 class="handwritten big">
                                        <a href="/program#zivotni-prostredi" class="black">ŽIVOTNÍ<br>PROSTŘEDÍ</a><br>
                                        <img src="{{ asset('images/rozdelovnik-cerny-2.png') }}" alt="rozdelovnik" width="100px">
                                    </h4>
                                </div>
                                <div class="col-sm-4 text-center mb-3">
                                    <h4 class="handwritten big">
                                        <a href="/program#spravedlnost" class="black">SPRAVE-<br>DLNOST</a><br>
                                        <img src="{{ asset('images/rozdelovnik-cerny-2.png') }}" alt="rozdelovnik" width="100px">
                                    </h4>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

                <div class="col-xl-4 text-center mb-5">
                    <facebook></facebook>
                </div>

                <div class="col-xl-8">
                    <h2 class="headline mb-4">Proč jsem se rozhodla kandidovat do Senátu?</h2>
                    <p>
                        Práci ombudsmanky jsem považovala za důstojnou tečku za svou veřejnou kariérou. Dělala jsem ji opravdu ráda, a myslím, že mi byla tak trochu souzena. Odcházela jsem s pocitem dobře odvedené práce a neměla jsem potřebu se poohlížet po další službě.
                    </p>
                    <p>
                        Začala jsem se ale ptát sama sebe: „Nemám přece ještě něco na tomto světě vykonat?“ A taky: „Co by tomu řekl můj táta?“ Tehdy jsem si uvědomila, že když se příležitost naskýtá, měla bych do toho jít. Přesto ještě nebylo rozhodnuto. Co mě přesvědčilo?
                    </p>
                    <p>
                        Cítím, že žijeme ve zlomové době, budoucnost je velmi nejistá a rozhodně nebude růžová. V posledních letech mám čím dál víc pocit, že lidstvo svou setrvačností podlamuje civilizaci. A že za to také nesu odpovědnost, protože nedělám vše, co je v mých silách, abychom na Zemi zachovali udržitelný život.
                    </p>
                    <p>
                        Celý svůj život se zajímám o politiku a sleduji společenské dění. Proto si uvědomuji, jak složitá a provázaná je naše civilizace a jak významnou roli hraje setrvačnost. Ale vidím kolem sebe také mnoho lidí, kteří věci vnímají podobně, byť s větší či menší naléhavostí. Dává to naději, že včas najdeme a přijmeme potřebná řešení. A to nakonec rozhodlo, že jsem se rozhodla přijmout výzvu a pokusit se ovlivňovat společenské dění jako senátorka.
                    </p>
                    <p>
                        Pokud nás čekají zásadní změny, a já jsem přesvědčena, že ano, musíme hlídat, aby probíhaly demokraticky, v dialogu a s respektem k jednotlivci. Na změnách musíme pracovat tak, abychom udrželi společenskou soudržnost. Aby naše demokracie nebyla jen formální, ale také sociálně spravedlivá, aby ji mohly podpořit všechny společenské skupiny. Sociální rozdíly se nesmějí zvětšovat, musíme je zmenšovat.
                    </p>
                    <p>
                        Senát je dobré místo, odkud lze střežit demokratický právní stát se vším, co to obnáší, a podporovat činnost důležitých institucí – například Ústavního soudu či veřejnoprávních médií.
                    </p>
                    <p>
                        Z naší společnosti se nesmí vytratit lidskost. Nežijeme proto, abychom pracovali, ale pracujeme proto, abychom mohli dobře žít. Žít ve vztazích a v harmonii s našimi blízkými. A to nejde bez péče. Péče o druhé, péče o blízké, děti, seniory, lidi s postižením. Péče v kvalitních institucích, ale také péče v domácnostech. Pečující proto musíme podpořit a ohodnotit, jak morálně, tak finančně.
                    </p>
                    <p>
                        Díky vaší důvěře a podpoře budu tato témata hájit s největším nasazením i v Senátu.
                    </p>
                    <p>
                        Anna Šabatová<br>
                        <img src="{{ asset('images/podpis-teal.png') }}" alt="podpis Anny Šabatové" width="33%" class="mt-5">
                    </p>
                </div>

                <div class="col-xl-4 text-center">
                    @include('layouts.endorsement')
                </div>
            </div>

        </div>

    </div>
@endsection
