<div class="col-xl-12 mt-5">
    <div class="card">
        <div class="card-header">
            <img src="{{ asset('/images/intro.jpg') }}" alt="intro" width="100%" height="auto">
        </div>

        <div class="card-body">
            <p>
                Pokračuji v práci - kandiduji do Senátu
            </p>

            <p>
                Vážení a milí, vážené a milé, celý svůj život jsem zasvětila hledání spravedlnosti.<br>
                Forma mých aktivit se měnila v závislosti na společenských a politických poměrech. Za diktatury před rokem ‘89 jsem společně s dalšími, prostřednictvím Výboru na obranu nespravedlivě stíhaných a Charty 77, upozorňovala na porušování lidských práv. Po revoluci jsem se lidským právům a ochraně občanů před nespravedlivým zacházením ze strany institucí věnovala dvanáct let v Úřadu veřejného ochránce práv – šest let jako zástupkyně ombudsmana Otakara Motejla, šest let jako ombudsmanka.
            </p>

            <p>
                Kandiduji, protože se nacházíme ve zlomovém období. Přicházejí zásadní společenské změny. Ty přinášejí výzvy, které nelze přehlížet.
                Chci proto nabídnout svoje celoživotní profesní i osobní zkušenosti a přispět k tomu, aby tyto změny neohrožovaly lidské svobody a důstojnost, aby byly citlivé k lidem i k přírodě. Cítím to jako svou povinnost a zodpovědnost.
            </p>

            <p>
                Senát je právě tím místem, kde mohu všechny svoje zkušenosti zúročit.<br>
                Věřím, že s vaší pomocí budu moct i nadále hájit vaše práva a důstojnost. Vytrvale a spravedlivě.
            </p>

        </div>

        <div class="card-footer">
            O mojí kandidatuře v médiích: <a href="https://www.info.cz/zpravodajstvi/cesko/sabatova-senat-kandidatura">Info.cz</a> | <a href="https://brnensky.denik.cz/zpravy_region/byvala-ombudsmanka-anna-sabatova-bude-kandidovat-do-senatu-20200722.html">Brněnský deník Rovnost</a> | <a href="https://www.ceskenoviny.cz/zpravy/byvala-ombudsmanka-sabatova-bude-kandidovat-do-senatu/1914681">ČTK</a>
        </div>
    </div>
</div>


<div class="col-xl-8">
    <h4 class="mt-5 mb-5 border-bottom-teal bigger">Aktuálně</h4>
    <div class="row mb-5">
        @foreach ($articles as $a)
            <div class="col-sm-4">
                <img src="{{ '/storage/'.$a->header }}" alt="{{ $a->slug }}" class="thumbnail">
            </div>

            <div class="col-sm-8">
                <a href="{{ route('article.show', ['article' => $a->slug]) }}">
                    <h4 class="teal">{{ $a->name }}</h4>
                </a>
                <p>
                    {!! Illuminate\Support\Str::limit($a->content, 180) !!}
                </p>
            </div>
        @endforeach
    </div>

    <h5 class="mt-5 mb-4 border-bottom-teal">Kdy a kde se můžeme vidět</h5>
    <div class="row mb-5">
        @foreach ($events as $e)
            @php
                $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $e->date);
            @endphp
            <div class="col-sm-12 mt-3 mb-3">
                <div class="row text-left">
                    <div class="col-sm-4">

                        <a href="{{ $e->facebook }}" class="handwritten teal-big" target="_blank">
                            {{ $date->format('d.m.') }}
                        </a>
                    </div>

                    <div class="col-sm-8">
                        <strong>{{ $e->place }}</strong><br>
                        {!! $e->content !!}
                    </div>
                </div>


            </div>
        @endforeach
    </div>

</div>
