@extends('layouts.app')

@section('title')
    Seznam událostí
@endsection

@section('content')
    <div class="container-xl">

        <div class="row mt-5 mb-5">

            <div class="col-sm-10">
                <h1 class="border-bottom-teal">Seznam událostí</h1>
            </div>

            <div class="col-sm-2 text-right">
                <a href="{{ route('admin.dashboard') }}">Zpět</a>&nbsp;
                <a href="{{ route('admin.event.create') }}" class="btn btn-primary">Vytvořit</a>
            </div>

            <div class="col-sm-12 mt-5">
                <table class="table table-hover">
                    <tr>
                        <th>Datum</th>
                        <th>Název</th>
                        <th>Místo</th>
                        <th class="text-right">Akce</th>
                    </tr>

                    @foreach ($events as $e)
                        <tr>
                            <td>
                                {{ date('d.m.Y H:i', strtotime($e->date)) }}
                            </td>
                            <td>{{ $e->content }}</td>
                            <td>{{ $e->place }}</td>
                            <td class="text-right">
                                <a href="{{ route('admin.event.edit', ['event' => $e->id]) }}" class="btn btn-secondary">Upravit</a>&nbsp;
                                <a href="{{ route('admin.event.delete', ['event' => $e->id]) }}" class="btn btn-danger">Smazat</a>
                            </td>
                        </tr>
                    @endforeach

                </table>
            </div>
        </div>
    </div>
@endsection

