@extends('layouts.app')

@section('title')
    Vytvořit událost
@stop

@section('content')

    <div class="container-xl">

        <div class="row mt-5 mb-5">

            <div class="col-sm-10">
                <h1 class="border-bottom-teal">Vytvořit událost</h1>
            </div>

            <div class="col-sm-2 text-right">
                <a href="{{ route('admin.event.index') }}" class="btn btn-secondary">Zpět</a>
            </div>

            <div class="col-sm-12 mt-5">
                <form method="POST">
                    @csrf

                    <div class="form-group row">
                        <div class="col-sm-3">
                            <label for="date">Datum</label>
                        </div>

                        <div class="col-sm-9">
                            <datetime type="datetime" name="date" class="theme-teal" format="yyyy-MM-dd HH:mm"></datetime>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-3">
                            <label for="place">Místo</label>
                        </div>

                        <div class="col-sm-9">
                            <input type="text" name="place" id="place" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label for="content-editor">Název</label>
                        </div>

                        <div class="col-sm-12">
                            <input type="text" name="content" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label for="facebook">Odkaz na FB</label>
                        </div>

                        <div class="col-sm-12">
                            <input type="text" name="facebook" id="facebook" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary">Uložit</button>

                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@stop
