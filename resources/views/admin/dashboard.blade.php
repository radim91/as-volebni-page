@extends('layouts.app')

@section('title')
    Dashboard
@endsection

@section('content')

    <div class="container-xl">
        <div class="row">

            <div class="col-sm-12 mt-3">
                <h2>Dashboard</h2>
            </div>

            <div class="col-sm-12 mt-5">
                <a href="{{ route('admin.volunteer.index') }}" class="bigger">Dobrovolníci</a><br>
                <a href="{{ route('admin.article.index') }}" class="bigger">Aktuality</a><br>
                <a href="{{ route('admin.event.index') }}" class="bigger">Události</a>
            </div>
        </div>

    </div>
@endsection
