@extends('layouts.app')

@section('title')
    Vytvořit aktualitu
@endsection

@section('content')

    <div class="container-xl">

        <div class="row mt-5">

            <div class="col-sm-10">
                <h1 class="border-bottom-teal">Vytvořit článek</h1>
            </div>

            <div class="col-sm-2 text-right">
                <a href="{{ route('admin.article.index') }}" class="btn btn-secondary">Zpět</a>
            </div>

            <div class="col-sm-12 mt-5">

                <form method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label for="name">Nadpis</label>
                        </div>

                        <div class="col-sm-12">
                            <input type="text" name="name" id="name" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label for="published">Publikovat v čase</label>
                        </div>

                        <div class="col-sm-12">
                            <input type="date" name="published" id="published" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label for="content-editor">Obsah</label>
                        </div>

                        <div class="col-sm-12">
                            <ckeditor/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-12">
                            <label for="header">Obrázek záhlaví</label>
                        </div>

                        <div class="col-sm-12">
                            <input type="file" name="header" required>
                        </div>
                    </div>

                    <div class="form-group row mt-5">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary">Uložit</button>

                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
@endsection
