@extends('layouts.app')

@section('title')
    Seznam aktualit
@endsection

@section('content')
    <div class="container-xl">

        <div class="row mt-5 mb-5">
            <div class="col-sm-10">
                <h1>Seznam článků</h1>
            </div>

            <div class="col-sm-2 text-right">
                <a href="{{ route('admin.dashboard') }}">Zpět</a>&nbsp;
                <a href="{{ route('admin.article.create') }}" class="btn btn-primary">Vytvořit</a>
            </div>

            <div class="col-sm-12 mt-5">
                <table class="table table-hover">
                    <tr>
                        <th>Publikace</th>
                        <th>Název</th>
                        <th class="text-right">Akce</th>
                    </tr>

                    @foreach ($articles as $a)
                        <tr>
                            <td>{{ $a->published }}</td>
                            <td>{{ $a->name }}</td>
                            <td class="text-right">
                                <a href="{{ route('admin.article.edit', ['article' => $a->slug]) }}" class="btn btn-secondary">Upravit</a>&nbsp;
                                <a href="{{ route('admin.article.delete', ['article' => $a->slug]) }}" class="btn btn-danger"
                                onclick="return confirm('Skutečně chcete smazat článek '.$a->name.' ?')">Smazat</a>
                            </td>
                        </tr>
                    @endforeach

                </table>
            </div>
        </div>
    </div>
@endsection
