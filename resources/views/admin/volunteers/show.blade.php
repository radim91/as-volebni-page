@extends('layouts.app')

@section('title')
    {{ decrypt($volunteer->name) }} {{ decrypt($volunteer->surname) }}
@endsection

@section('content')

    <div class="container-xl">

        <div class="row">

            <div class="col-xl-10 mt-5">
                <h2>Záznam dobrovolníka</h2>
            </div>

            <div class="col-xl-2 mt-5 text-right">
                <a href="{{ route('admin.volunteer.index') }}" class="btn btn-secondary">Zpět</a>
            </div>

            <div class="col-xl-12 mt-5">
                <table class="table table-hover">
                    <tr>
                        <th>Jméno</th>
                        <td>{{ decrypt($volunteer->name) }} {{ decrypt($volunteer->surname) }}</td>
                    </tr>

                    <tr>
                        <th>E-mail</th>
                        <td>{{ $volunteer->email }}</td>
                    </tr>

                    @if (decrypt($volunteer->phone) != null)
                        <tr>
                            <th>Telefon</th>
                            <td>{{ decrypt($volunteer->phone) }}</td>
                        </tr>
                    @endif

                    @if (decrypt($volunteer->address) != null)
                        <tr>
                            <th>Adresa</th>
                            <td>{{ decrypt($volunteer->address) }}</td>
                        </tr>
                    @endif

                    <tr></tr>

                    @foreach (json_decode($volunteer->option_value, false) as $key => $value)
                        @foreach ($value as $k => $v)
                            <tr>
                                <th>{{ $k }}</th>
                                <td>
                                    @if ($v == 'on')
                                        <span class="text-success">✓</span>
                                    @else
                                        {{ $v }}
                                    @endif
                                </td>
                            </tr>

                        @endforeach
                    @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection
