@extends('layouts.app')

@section('title')
    Výpis dobrovolníků
@endsection

@section('content')

    <div class="container-xl">

        <div class="row justify-content-center mt-5">
            <div class="col-xl-10">
                <h1 class="mb-5">Výpis dobrovolníků</h1>
            </div>
            <div class="col-xl-2 text-right">
                <a href="{{ route('admin.volunteer.index') }}">Zpět</a>&nbsp;
                <button id="excel" class="btn btn-secondary">Excel</button>
            </div>

            <div class="col-xl-12">
                <table class="table table-hover table-responsive table-bordered" id="tabulka">
                    <tr>
                        <th>Jméno</th>
                        <th>E-mail</th>
                        <th>Plakát</th>
                        <th>Známí</th>
                        <th>Samolepka</th>
                        <th>Jídlo</th>
                        <th>Plachta</th>
                        <th>Kontaktní místo</th>
                        <th>Letáky</th>
                        <th>Stánek</th>
                        <th>Dveře</th>
                        <th>Akce</th>
                        <th>Jiné</th>
                    </tr>

                    @foreach($volunteers as $v)
                        @php
                            $help = json_decode($v->option_value, true);
                            $helpArray = [];
                            foreach ($help as $value => $key) {
                                foreach ($key as $name => $val) {
                                    $helpArray[$name] = $val;
                                }
                            }
                        @endphp
                        <tr>
                            <td>
                                <a href="{{ route('admin.volunteer.show', ['volunteer' => $v->id]) }}">
                                    <strong>
                                        {{ decrypt($v->surname).' '.decrypt($v->name) }}
                                    </strong>
                                </a>
                            </td>
                            <td>{{ $v->email }}</td>
                            <td>
                                @if (isset($helpArray['pomoc_plakat']))
                                    x
                                @endif
                            </td>
                            <td>
                                @if (isset($helpArray['pomoc_znami']))
                                    x
                                @endif
                            </td>
                            <td>
                                @if (isset($helpArray['pomoc_samolepka']))
                                    x
                                @endif
                            </td>
                            <td>
                                @if (isset($helpArray['pomoc_jidlo']))
                                    x
                                @endif
                            </td>
                            <td>
                                @if (isset($helpArray['pomoc_plachta']))
                                    @if (isset($helpArray['pomoc_plachta_adr']))
                                        {{ $helpArray['pomoc_plachta_adr'] }}
                                    @else
                                        x
                                    @endif
                                @endif
                            </td>
                            <td>
                                @if (isset($helpArray['pomoc_kontaktni_misto']))
                                    @if (isset($helpArray['pomoc_kontaktni_misto_adr']))
                                        {{ $helpArray['pomoc_kontaktni_misto_adr'] }}
                                    @else
                                        x
                                    @endif
                                @endif
                            </td>
                            <td>
                                @if (isset($helpArray['pomoc_letaky']))
                                    @if (isset($helpArray['pomoc_letaky_adr']))
                                        {{ $helpArray['pomoc_letaky_adr'] }}
                                    @else
                                        x
                                    @endif
                                @endif
                            </td>
                            <td>
                                @if (isset($helpArray['pomoc_stanek']))
                                    @if (isset($helpArray['pomoc_stanek_adr']))
                                        {{ $helpArray['pomoc_stanek_adr'] }}
                                    @else
                                        x
                                    @endif
                                @endif
                            </td>
                            <td>
                                @if (isset($helpArray['pomoc_dvere']))
                                    @if (isset($helpArray['pomoc_dvere_adr']))
                                        {{ $helpArray['pomoc_dvere_adr'] }}
                                    @else
                                        x
                                    @endif
                                @endif
                            </td>
                            <td>
                                @if (isset($helpArray['pomoc_akce']))
                                    @if (isset($helpArray['pomoc_akce_adr']))
                                        {{ $helpArray['pomoc_akce_adr'] }}
                                    @else
                                        x
                                    @endif
                                @endif
                            </td>
                            <td>
                                @if (isset($helpArray['pomoc_jine']))
                                    @if (isset($helpArray['pomoc_jine_detail']))
                                        {{ $helpArray['pomoc_jine_detail'] }}
                                    @else
                                        x
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach

                </table>
            </div>
        </div>
    </div>
@endsection
