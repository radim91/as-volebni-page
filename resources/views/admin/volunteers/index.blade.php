@extends('layouts.app')

@section('title')
    Seznam dobrovolníků
@endsection

@section('content')

    <div class="container-xl">

        <div class="row mt-5 mb-5">
            <div class="col-sm-10">
                <h2>Seznam dobrovolníků</h2>
            </div>

            <div class="col-sm-2 text-right">
                <a href="{{ route('admin.dashboard') }}">Zpět</a>&nbsp;
                <a href="{{ route('admin.volunteer.extended') }}" class="btn btn-secondary">Výpis</a>
            </div>
        </div>

        @if (session('status'))
            <div class="alert alert-success mt-5">
                {{ session('status') }}
            </div>
        @endif

        <div class="col-sm-12 mt-5">
            <table class="table table-hover">
                <tr>
                    <th>Vytvořeno</th>
                    <th>Jméno</th>
                    <th>Email</th>
                    <th class="text-right">Akce</th>
                </tr>
                @foreach ($volunteers as $v)
                    <tr>
                        <td>{{ $v->created_at }}</td>
                        <td>{{ decrypt($v->name) }} {{ decrypt($v->surname) }}</td>
                        <td>{{ $v->email }}</td>
                        <td class="text-right">
                            <a href="{{ route('admin.volunteer.show', ['volunteer' => $v->id]) }}" class="btn btn-primary">Detail</a>&nbsp;
                            <a
                                href="{{ route('admin.volunteer.delete', ['volunteer' => $v->id]) }}"
                                class="btn btn-danger"
                                onclick="return confirm('Skutečně chcete smazat dobrovolníka {{ decrypt($v->name) }} {{ decrypt($v->surname) }}?')"
                            >Smazat</a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
