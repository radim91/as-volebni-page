@extends('layouts.app')

@section('title')
    Proč jsem se rozhodla kandidovat do Senátu?
@endsection

@section('content')
    <div class="container-xl">

        <div class="row">
            <div class="col-xl-8">
                <img src="{{ asset('images/procsenat.png') }}" alt="proč senát" class="page-head">
            </div>
        </div>

        <div class="row mt-5 mb-5">

            <div class="col-xl-7">
                <p>
                    Práci ombudsmanky jsem považovala za důstojnou tečku za svou veřejnou kariérou. Dělala jsem ji opravdu ráda, a myslím, že mi byla tak trochu souzena. Odcházela jsem s pocitem dobře odvedené práce a neměla jsem potřebu se poohlížet po další službě.
                </p>
                <p>
                    Začala jsem se ale ptát sama sebe: „Nemám přece ještě něco na tomto světě vykonat?“ A taky: „Co by tomu řekl můj táta?“ Tehdy jsem si uvědomila, že když se příležitost naskýtá, měla bych do toho jít. Přesto ještě nebylo rozhodnuto. Co mě přesvědčilo?
                </p>
                <p>
                    Cítím, že žijeme ve zlomové době, budoucnost je velmi nejistá a rozhodně nebude růžová. V posledních letech mám čím dál víc pocit, že lidstvo svou setrvačností podlamuje civilizaci. A že za to také nesu odpovědnost, protože nedělám vše, co je v mých silách, abychom na Zemi zachovali udržitelný život.
                </p>
                <p>
                    Celý svůj život se zajímám o politiku a sleduji společenské dění. Proto si uvědomuji, jak složitá a provázaná je naše civilizace a jak významnou roli hraje setrvačnost. Ale vidím kolem sebe také mnoho lidí, kteří věci vnímají podobně, byť s větší či menší naléhavostí. Dává to naději, že včas najdeme a přijmeme potřebná řešení. A to nakonec rozhodlo, že jsem se rozhodla přijmout výzvu a pokusit se ovlivňovat společenské dění jako senátorka.
                </p>
                <p>
                    Pokud nás čekají zásadní změny, a já jsem přesvědčena, že ano, musíme hlídat, aby probíhaly demokraticky, v dialogu a s respektem k jednotlivci. Na změnách musíme pracovat tak, abychom udrželi společenskou soudržnost. Aby naše demokracie nebyla jen formální, ale také sociálně spravedlivá, aby ji mohly podpořit všechny společenské skupiny. Sociální rozdíly se nesmějí zvětšovat, musíme je zmenšovat.
                </p>
                <p>
                    Senát je dobré místo, odkud lze střežit demokratický právní stát se vším, co to obnáší, a podporovat činnost důležitých institucí – například Ústavního soudu či veřejnoprávních médií.
                </p>
                <p>
                    Z naší společnosti se nesmí vytratit lidskost. Nežijeme proto, abychom pracovali, ale pracujeme proto, abychom mohli dobře žít. Žít ve vztazích a v harmonii s našimi blízkými. A to nejde bez péče. Péče o druhé, péče o blízké, děti, seniory, lidi s postižením. Péče v kvalitních institucích, ale také péče v domácnostech. Pečující proto musíme podpořit a ohodnotit, jak morálně, tak finančně.
                </p>
                <p>
                    Díky vaší důvěře a podpoře budu tato témata hájit s největším nasazením i v Senátu.
                </p>

                <img src="{{ asset('images/podpis-teal.png') }}" alt="podpis Anny Šabatové" width="33%" class="mt-5">
            </div>

            <div class="col-xl-5 mb-5 text-center">
                <div class="py-4">
                    @include('layouts.buttons')
                </div>

                <div class="mt-5">
                    <facebook></facebook>
                </div>

                <div class="mt-5 mt-5">
                    @include('layouts.endorsement')
                </div>
            </div>
        </div>
    </div>
@endsection
