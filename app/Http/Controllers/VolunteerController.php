<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Volunteer;

class VolunteerController extends Controller
{

    public function index()
    {
        return view('volunteer');
    }

    public function save(Request $request)
    {
        if (Volunteer::validateEntry($request)) {
            Volunteer::create([
                'name' => encrypt($request->name),
                'surname' => encrypt($request->surname),
                'email' => $request->email,
                'phone' => encrypt($request->phone),
                'address' => encrypt($request->address),
                'option_value' => Volunteer::jsonize($request->all())
            ]);

            return redirect('/')->with('status', 'OK');
        } else {
            return back()->with('error', 'error');
        }
    }
}
