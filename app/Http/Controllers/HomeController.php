<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function welcome() {
        $articles = DB::table('articles')
            ->where('updated_at', '<', date('Y-m-d H:i:s'))
            ->orderBy('updated_at', 'DESC')
            ->limit(3)
            ->get();

        $events = DB::table('events')
            ->orderBy('updated_at', 'DESC')
            ->limit(4)
            ->get();

        return view('welcome', [
            'articles' => $articles,
            'events' => $events
        ]);
    }

    public function index()
    {
        return view('home');
    }

    public function procSenat() {
        return view('procsenat');
    }

    public function program() {
        return view('program');
    }

    public function pribeh() {
        return view('pribeh');
    }

    public function podpora() {
        return view('podpora');
    }
}
