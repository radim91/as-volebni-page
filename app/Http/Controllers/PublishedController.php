<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Event;

class PublishedController extends Controller
{
    public function showArticle(Article $article) {
        return view('showarticle', ['article' => $article]);
    }
}
