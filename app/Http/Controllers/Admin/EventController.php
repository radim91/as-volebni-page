<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Event;

class EventController extends \App\Http\Controllers\AdminController
{
    public function index () {
        return view ('admin/events/index', [
            'events' => DB::table('events')->orderBy('date', 'DESC')->get()
        ]);
    }

    public function create () {
        return view ('admin/events/create');
    }

    public function store (Request $request) {
        $event = Event::createEvent($request);

        if ($event != null) {
            return redirect()->route('admin.event.index', ['id' => $event->id])->with('status', 'Aktualita úspěšně vytvořena.');
        } else {
            return redirect()->route('admin.event.index')->with('error', 'Aktualitu se nepodařilo uložit.');
        }
    }

    public function edit (Event $event) {
        return view('admin.events.edit', [
            'event' => $event
        ]);
    }

    public function update (Request $request, Event $event) {
        if (Event::editEvent($request, $event)) {
            return redirect()->route('admin.event.index', ['id' => $event->id])->with('status', 'Událost úspěšně upravena.');
        } else {
            return redirect()->route('admin.event.index')->with('error', 'Událost se nepodařilo uložit.');
        }
    }

    public function delete (Event $event) {
        $event->delete();
        return redirect()->route('admin.event.index')->with('status', 'Událost byla úspěšně smazána.');
    }
}
