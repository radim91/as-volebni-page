<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Volunteer;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;


class VolunteerController extends Controller
{
    public function index(): View {
        $volunteers = Volunteer::all();
        return view('admin/volunteers/index', ['volunteers' => $volunteers]);
    }

    public function extended():View {
        $volunteers = Volunteer::all();
        return view('admin/volunteers/extended', ['volunteers' => $volunteers]);
    }

    public function show(Volunteer $volunteer): View {
        return view('admin/volunteers/show', ['volunteer' => $volunteer]);
    }

    public function delete(Volunteer $volunteer): RedirectResponse {
        $volunteer->delete();
        return redirect()->route('admin.volunteer.index')->with('status', 'Dobrovolníka se podařilo smazat.');
    }
}
