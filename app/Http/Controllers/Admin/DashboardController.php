<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;


use App\Volunteer;

class DashboardController extends AdminController
{
    public function index() {
        return view('admin/dashboard');
    }
}
