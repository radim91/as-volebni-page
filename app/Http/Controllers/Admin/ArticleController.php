<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\AdminController;
use App\Article;

class ArticleController extends AdminController
{
    public function index () {
        return view ('admin/articles/index', [
            'articles' => DB::table('articles')->orderBy('updated_at', 'DESC')->get(),
        ]);
    }

    public function create () {
        return view ('admin/articles/create');
    }

    public function store (Request $request) {
        $article = Article::createArticle($request);

        if ($article != null) {
            return redirect()->route('admin.article.index', ['slug' => $article->slug])->with('status', 'Aktualita úspěšně vytvořena.');
        } else {
            return redirect()->route('admin.article.index')->with('error', 'Aktualitu se nepodařilo uložit.');
        }
    }

    public function edit (Article $article) {
        return view ('admin/articles/edit', [
           'article' => $article
        ]);
    }

    public function delete (Article $article) {
        $article->delete();
        return redirect()->route('admin.article.index')->with('status', 'Aktualita byla úspěšně smazána.');
    }
}
