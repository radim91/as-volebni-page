<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;

trait Upload {
    public static function uploadFile($folder, $file, $name)
    {
        $path = Storage::putFileAs(
            'public/' . $folder,
            $file,
            $name,
            'public'
        );

        //zbavim se 'public'
        $explodedPath = explode("/", $path, 2);

        return $explodedPath[1];
    }

    public static function uploadMultiple($folder, $files)
    {
        $paths = [];

        foreach ($files as $f) {
            $p = Storage::putFileAs(
                'public/' . $folder,
                $f,
                $f->getClientOriginalName(),
                'public'
            );

            $explodedPath = explode("/", $p, 2);

            array_push($paths, $explodedPath);
        }

        return json_encode($paths);
    }
}
