<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use App\Traits\Upload;

/**
 * App\Article
 *
 * @property int $id
 * @property string $slug
 * @property string $name
 * @property string $published
 * @property string $content
 * @property string $header
 * @property string|null $links
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Article newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Article newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Article query()
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereHeader($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereLinks($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Article whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Article extends Model
{
    protected $guarded = ['id'];
    private const BASERULES = [
        'name' => ['required', 'max:255'],
        'published' => ['required', 'date'],
        'content' => ['required'],
        'links' => ['nullable', 'json']
    ];

    public static function validation ($request, $method) {
        if ($method == 'create') {
            return $request->validate(array_merge(
                self::BASERULES,
                ['header' => ['required', 'image']]
            ));
        } else {
            return $request->validate(array_merge(
                self::BASERULES,
                ['header' => ['nullable', 'image']]
            ));
        }
    }

    public static function createArticle (Request $request) {
        if (Article::validation($request, 'create')) {
            $slug = Str::of($request->name)->slug('-');
            if (Article::where('slug', $slug)->count() > 0) {
                return null;
            } else {
                $data = array_merge($request->all(), ['slug' => $slug]);
                $data['header'] = Upload::uploadFile('articles', $request->header, $slug.'.'.$request->header->getClientOriginalExtension());
                return Article::create($data);
            }

        } else {
            return null;
        }
    }

    public static function editArticle (Request $request, Article $article) {

    }
}
