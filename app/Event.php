<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * App\Event
 *
 * @property int $id
 * @property string $date
 * @property string $place
 * @property string $description
 * @property string|null $facebook
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Event newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Event newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Event query()
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereFacebook($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event wherePlace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Event extends Model
{
    protected $guarded = ['id'];
    private const BASERULES = [
        'date' => ['required', 'date'],
        'place' => ['required'],
        'content' => ['required'],
        'facebook' => ['nullable']
    ];

    public static function validation ($request) {
        return $request->validate(self::BASERULES);
    }

    public static function createEvent (Request $request) {
        if (Event::validation($request)) {
            return Event::create($request->all());
        }
    }

    public static function editEvent (Request $request, Event $event) {
        if (Event::validation($request)) {
            return $event->update($request->all());
        }
    }
}
