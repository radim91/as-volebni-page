<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * App\Volunteer
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $name
 * @property string $surname
 * @property string $email
 * @property string|null $phone
 * @property string $address
 * @property string $option_value
 * @method static \Illuminate\Database\Eloquent\Builder|Volunteer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Volunteer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Volunteer query()
 * @method static \Illuminate\Database\Eloquent\Builder|Volunteer whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Volunteer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Volunteer whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Volunteer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Volunteer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Volunteer whereOptionValue($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Volunteer wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Volunteer whereSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Volunteer whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Volunteer extends Model
{
    protected $guarded = ['id'];

    private const BASERULES = [
        'name' => ['required', 'max:255'],
        'surname' => ['required', 'max: 255'],
        'email' => ['unique:volunteers', 'required', 'email'],
    ];

    public static function validateEntry($request)
    {
        return $request->validate(self::BASERULES);
    }

    public static function jsonize($request)
    {
        $data = [];

        foreach ($request as $key => $value) {
            if (substr($key, 0, 5) == 'pomoc' && ($value != null && $value != "Jaký?")) {
                array_push($data, [$key => $value]);
            }
        }

        return json_encode($data);
    }
}
