ALTER TABLE sabatova.events CHANGE description content text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;
ALTER TABLE sabatova.events MODIFY COLUMN `date` DATETIME NOT NULL;
