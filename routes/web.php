<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@welcome')->name('welcome');

Auth::routes(['register' => false]);

//STATIC
Route::get('/proc-senat', 'HomeController@procSenat')->name('procsenat');
Route::get('/program', 'HomeController@program')->name('program');
Route::get('/vize', 'HomeController@program')->name('vize');
Route::get('/pribeh', 'HomeController@pribeh')->name('pribeh');
Route::get('/podpora', 'HomeController@podpora')->name('podpora');
Route::get('/dary', function () {
    return redirect('https://dary.zeleni.cz/annasabatova?utm_campaign=annasabatova&utm_medium=facebook&utm_source=vyzva');
});

//VOLUNTEERS
Route::get('/dobrovolnik', 'VolunteerController@index')->name('volunteer');
Route::post('/dobrovolnik', 'VolunteerController@save');

//ARTICLES
Route::get('/clanek/{article:slug}', 'PublishedController@showArticle')->name('article.show');

//EVENTS

//ADMIN
Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function () {
    //dahsboard
    Route::get('/dashboard', 'DashboardController@index')->name('admin.dashboard');

    //volunteers
    Route::get('/dobrovolnik/', 'VolunteerController@index')->name('admin.volunteer.index');
    Route::get('/dobrovolnik/vypis', 'VolunteerController@extended')->name('admin.volunteer.extended');
    Route::get('/dobrovolnik/detail/{volunteer:id}', 'VolunteerController@show')->name('admin.volunteer.show');
    Route::get('/dobrovolnik/smazat/{volunteer:id}', 'VolunteerController@delete')->name('admin.volunteer.delete');

    //articles
    Route::group(['prefix' => 'clanek'], function () {
        Route::get('/', 'ArticleController@index')->name('admin.article.index');
       Route::get('/vytvorit', 'ArticleController@create')->name('admin.article.create');
       Route::post('/vytvorit', 'ArticleController@store');
       Route::get('upravit/{article:slug}', 'ArticleController@edit')->name('admin.article.edit');
       Route::post('upravit/{article:slug}', 'ArticleController@update')->name('admin.article.update');
       Route::get('smazat/{article:slug}', 'ArticleController@delete')->name('admin.article.delete');
    });

    //events
    Route::group(['prefix' => 'udalost'], function () {
        Route::get('/', 'EventController@index')->name('admin.event.index');
        Route::get('/vytvorit', 'EventController@create')->name('admin.event.create');
        Route::post('/vytvorit', 'EventController@store')->name('admin.event.store');
        Route::get('upravit/{event:id}', 'EventController@edit')->name('admin.event.edit');
        Route::post('upravit/{event:id}', 'EventController@update');
        Route::get('smazat/{event:id}', 'EventController@delete')->name('admin.event.delete');
    });

    Route::group(['prefix' => 'upload'], function () {
        \UniSharp\LaravelFilemanager\Lfm::routes();
    });
});
